#pragma once
#ifndef LABI_A_G_MAT_H
#define LABI_A_G_MAT_H

#include <cstring>

template<typename T>
class Mat {
private:
    int cols;
    int rows;
    T *M;

public:
    Mat(int rows, int cols);

    Mat(int rows, int cols, const T &value);

    Mat(const Mat<T> &m);

    ~Mat();

    /*T &operator()(int row, int col) {
        return M[row * cols + col];
    }

    const T &operator()(int row, int col) const {
        return M[row * cols + col];
    }*/

    T &operator()(int row, int col);

    const T &operator()(int row, int col) const;

    /*inline T &operator()(int row, int col) {
        return M[row * cols + col];
    }

    inline const T &operator()(int row, int col) const {
        return M[row * cols + col];
    }*/

    Mat<T> &operator=(const Mat<T> &m);

    Mat<T> &operator=(const T &s);

    Mat operator+(const Mat<T> &b);

    Mat operator+(const T &s);

    Mat operator-(const Mat<T> &b);

    Mat operator-(const T &s);

    Mat operator*(const T &s);

    Mat operator*(const Mat<T> &a);

    bool operator==(const Mat<T> &b);

    Mat<T> &operator=(Mat<T> &m);

    Mat<T> &operator=(T &s);

    Mat operator+(Mat<T> &b);

    Mat operator+(T &s);

    Mat operator-(Mat<T> &b);

    Mat operator-(T &s);

    Mat operator*(T &s);

    Mat operator*(Mat<T> &a);

    bool operator==(Mat<T> &b);


    Mat<T> t();

    double det();

    static Mat<T> eye(int rows_and_cols);

    static Mat<T> zeros(int rows, int cols);
};

#endif //LABI_A_G_MAT_H