#include <iostream>
#include "Mat.h"

using namespace std;

int main() {
    int x = 10;
    int y = 10;
    Mat<int> matrix_2 = Mat<int>(x, y, 5);
    matrix_2(1, 2) = 1;
    matrix_2(3, 5) = 1;
    for (int i = 0; i < x; i++) {
        for (int j = 0; j < y; j++) {
            cout << matrix_2(i, j) << " ";
        }
        cout << endl;
    }

    return 0;
}
