#include "Mat.h"

template<typename T>
Mat<T>::Mat(int rows, int cols): Mat(rows, cols, 0) {}

template<typename T>
Mat<T>::Mat(int rows, int cols, const T &value) {
    M = (T *) new T[rows * cols];
    for (int i = 0; i < rows * cols; i++) {
        M[i] = value;
    }
}

template<typename T>
Mat<T>::Mat(const Mat &m):cols(m.cols), rows(m.rows) {
    M = (T *) new T[rows * cols];
    for (int i = 0; i < cols * rows; ++i) {
        M[i] = m.M[i];
    }
}

template<typename T>
Mat<T>::Mat(Mat &m):cols(m.cols), rows(m.rows) {
    M = (T *) new T[rows * cols];
    for (int i = 0; i < cols * rows; ++i) {
        M[i] = m.M[i];
    }
}

template<typename T>
Mat<T>::~Mat() {
    delete[] M;
}

template<typename T>
T &Mat<T>::operator()(int row, int col) {
    return M[row * cols + col];
}

template<typename T>
const T &Mat<T>::operator()(int row, int col) const {
    return M[row * cols + col];
}

/*template<typename T>
inline T &operator()(int row, int col) {
    return M[row * cols + col];
}

template<typename T>
inline const T &operator()(int row, int col) const {
    return M[row * cols + col];
}*/

template<typename T>
Mat<T> Mat<T>::zeros(int rows, int cols) {
    return Mat<int>(rows, cols, 0);
}

template<typename T>
Mat<T> Mat<T>::eye(int rows_and_cols) {
    Mat<int> Eye(rows_and_cols, rows_and_cols, 0);
    for (int i = 0; i < rows_and_cols; ++i) {
        Eye(i, i) = 1;
    }
    return Eye;
}

template<typename T>
bool Mat<T>::operator==(const Mat<T> &b) {
    if (cols != b.cols || rows != b.rows) {
        return false;
    }
    for (int i = 0; i < b.cols * b.rows; i++) {
        if (M[i] != b.M[i]) {
            return false;
        }
    }
    return true;
}

template<typename T>
bool Mat<T>::operator==(Mat<T> &b) {
    if (cols != b.cols || rows != b.rows) {
        return false;
    }
    for (int i = 0; i < b.cols * b.rows; i++) {
        if (M[i] != b.M[i]) {
            return false;
        }
    }
    return true;
}

template<typename T>
Mat<T> Mat<T>::operator*(const Mat<T> &b) {
    Mat<T> this_m(*this);
    Mat<T> result_m(rows, b.cols, 0);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < b.cols; j++) {
            for (int p = 0; p < cols; p++) {
                result_m(i, j) += this_m(i, p) * b(p, j);
            }
        }
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator*(Mat<T> &b) {
    Mat<T> this_m(*this);
    Mat<T> result_m(rows, b.cols, 0);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < b.cols; j++) {
            for (int p = 0; p < cols; p++) {
                result_m(i, j) += this_m(i, p) * b(p, j);
            }
        }
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator*(const T &s) {
    Mat<T> this_m(*this);
    Mat<T> result_m(this_m);
    for (int i = 0; i < rows * cols; i++) {
        result_m[i] *= s;
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator*(T &s) {
    Mat<T> this_m(*this);
    Mat<T> result_m(this_m);
    for (int i = 0; i < rows * cols; i++) {
        result_m[i] *= s;
    }
    return result_m;
}

template<typename T>
Mat<T> &Mat<T>::operator=(const Mat<T> &m) {
    rows = m.rows;
    cols = m.cols;
    M = new T[rows * cols];
    for (int i = 0; i < rows * cols; ++i) {
        M[i] = m.M[i];
    }
    return *this;
}

template<typename T>
Mat<T> &Mat<T>::operator=(Mat<T> &m) {
    rows = m.rows;
    cols = m.cols;
    M = new T[rows * cols];
    for (int i = 0; i < rows * cols; ++i) {
        M[i] = m.M[i];
    }
    return *this;
}

template<typename T>
Mat<T> &Mat<T>::operator=(const T &s) {
    for (int i = 0; i < rows * cols; ++i) {
        M[i] = s;
    }
    return *this;
}

template<typename T>
Mat<T> &Mat<T>::operator=(T &s) {
    for (int i = 0; i < rows * cols; ++i) {
        M[i] = s;
    }
    return *this;
}

template<typename T>
Mat<T> Mat<T>::operator+(const Mat<T> &b) {
    Mat<T> result_m(b);
    if (rows == b.rows && cols == b.cols) {
        for (int i = 0; i < cols * rows; ++i) {
            result_m.M[i] += M[i];
        }
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator+(Mat<T> &b) {
    Mat<T> result_m(b);
    if (rows == b.rows && cols == b.cols) {
        for (int i = 0; i < cols * rows; ++i) {
            result_m.M[i] += M[i];
        }
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator-(const Mat<T> &b) {
    Mat<T> result_m(b);
    if (rows == b.rows && cols == b.cols) {
        for (int i = 0; i < cols * rows; ++i) {
            result_m.M[i] += M[i];
        }
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator-(Mat<T> &b) {
    Mat<T> result_m(b);
    if (rows == b.rows && cols == b.cols) {
        for (int i = 0; i < cols * rows; ++i) {
            result_m.M[i] += M[i];
        }
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator+(const T &s) {
    Mat<T> result_m(rows, cols, s);
    for (int i = 0; i < cols * rows; ++i) {
        result_m.M[i] += M[i];
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator+(T &s) {
    Mat<T> result_m(rows, cols, s);
    for (int i = 0; i < cols * rows; ++i) {
        result_m.M[i] += M[i];
    }
    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator-(const T &s) {
    Mat<T> result_m(rows, cols, s);
    for (int i = 0; i < cols * rows; ++i) {
        result_m.M[i] = M[i] - result_m.M[i];
    }

    return result_m;
}

template<typename T>
Mat<T> Mat<T>::operator-(T &s) {
    Mat<T> result_m(rows, cols, s);
    for (int i = 0; i < cols * rows; ++i) {
        result_m.M[i] = M[i] - result_m.M[i];
    }

    return result_m;
}

template<typename T>
Mat<T> Mat<T>::t() {
    Mat<T> result_m = Mat(rows, cols);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            result_m.M[i * rows + j] = M[j * cols + i];
        }
    }
    return result_m;
}

template<typename T>
double Mat<T>::det() {
    if (cols == rows) {
        int non_zero_row = 0, current_row = 0;
        for (int current_column = 0; current_column < cols; ++current_column) {

            int column_consists_of_zeros = 1;
            for (int row = current_row; row < rows; ++row) {
                if (M[row * rows + current_column] != 0) {
                    column_consists_of_zeros = 0;
                    non_zero_row = row;
                    break;
                }
            }
            if (column_consists_of_zeros == 1) {
                continue;
            }

            double non_zero_element = M[non_zero_row][current_column];
            if (non_zero_row != current_row) {
                for (int column = 0; column < cols; ++column) {
                    M[current_column][column] += M[non_zero_row][column];
                }
            }

            for (int row = current_row + 1; row < rows; ++row) {
                double first_element_of_current_row = M[row][current_column];
                for (int column = 0; column < cols; ++column) {
                    M[row][column] += (-(first_element_of_current_row / non_zero_element)) * M[current_row][column];
                }
            }
            ++current_row;
        }

        double determinant = 1;
        for (int diagonal_index = 0; diagonal_index < rows; ++diagonal_index) {
            determinant *= M[diagonal_index][diagonal_index];
        }
        return determinant;
    }
    return 0;
}
